# Go Timey Client

This is a small utility to quickly read from a timey server.

## Setup

You need to set two enviroment variables:

``` bash

export TIMEY_KEY=     #your timey api key
export TIMEY_SERVER=  #your timey server ip / domain

```