package location

import (
	"sync"
	"time"
)

type locHolder struct {
	mu       sync.Mutex
	loc      *time.Location
	location string
}

func NewLocHolder(loc string) locHolder {
	return locHolder{
		location: loc,
	}
}

// GetTime lazy loads and returns the specified location.
// error is set, if the given location is invalid.
func (l *locHolder) GetTime() (*time.Location, error) {
	if l.loc == nil {
		loc, err := time.LoadLocation(l.location)
		if err != nil {
			return nil, err
		}
		l.loc = loc
	}

	return l.loc, nil
}
