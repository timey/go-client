package main

import (
	"encoding/json"
	"fmt"
	"fredlahde.com/TimeyClient/location"
	log "github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"net/http"
	"time"
	"os"
)

type Time struct {
	Start       int64  `json:"start"`
	End         int64  `json:"end"`
	Description string `json:"description"`
}

var locHolder = location.NewLocHolder("Europe/Berlin")

func (t Time) String() string {
	loc, err := locHolder.GetTime()
	if err != nil {
		log.Fatal(err)
	}
	dateStart := time.Unix(t.Start, 0).In(loc)
	dateEnd := time.Unix(t.End, 0).In(loc)

	return fmt.Sprintf("%v -> %v\t%s", dateStart, dateEnd, t.Description)
}

func getEnv(key string) string {
	value := os.Getenv(key)
	if value == "" {
		log.Fatalf("could not read %s from environment.", key)
	}
	return value
}

func main() {
	key := getEnv("TIMEY_KEY")
	server := getEnv("TIMEY_SERVER")

	var netClient = &http.Client{
		Timeout: time.Second * 10,
	}

	req, err := http.NewRequest("GET", "http://"+server+"/api/times", nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Authorization", "Bearer "+key)
	r, err := netClient.Do(req)

	if err != nil {
		log.Fatal(err)
	}

	if r.StatusCode != 200 {
		log.Fatalf("could not read from %s, got: %s", server, r.Status)
	}

	body := r.Body
	closer := func(body io.ReadCloser) {
		err := body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}

	s, err := ioutil.ReadAll(body)
	if err != nil {
		log.Fatal(err)
	}

	data := make([]Time, 0)

	if err = json.Unmarshal(s, &data); err != nil {
		log.Fatal(err)
	}

	for i, t := range data {
		fmt.Printf("%d\t%s\n", i+1, t)
	}
	closer(body)
}
